/*
 * @Author: fbt fbtgold@foxmail.com
 * @Date: 2023-11-28 14:55:51
 * @LastEditors: fbt fbtgold@foxmail.com
 * @LastEditTime: 2023-11-28 15:01:38
 * @FilePath: \untitled\test\grammer\base.dart
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
void main() {
    double a = 10.5;
    double b = 20.9;
    double c = 30.4;

    double result = (a + b + c) / 3;
    print(result);
}