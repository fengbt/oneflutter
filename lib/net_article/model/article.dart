class Article {
  final String title;
  final String url;
  final String time;

  Article({
    required this.title,
    required this.url,
    required this.time
  });

  // 通过formMap构造Article对象
  factory Article.formMap(dynamic map) {
    return Article(
        title: map['title'] ?? '未知',
        url: map['link'] ?? '未知',
        time: map['niceDate'] ?? '未知'
    );
  }

  @override
  String toString(){
    return 'Article{title: $title,url :$url,time: $time}';
  }
}