import 'package:sqflite/sqflite.dart';
import 'package:untitled/muyu/models/merit_record.dart';

class MeritRecordDao{
  final Database database;

  MeritRecordDao(this.database);

  static String tableName = 'merit_record';

  static String tableSql = """
  CREATE TABLE $tableName (
    id VARCHAR(64) PRIMARY KEY,
    value INTEGER,
    image TEXT,
    audio TEXT,
    timestamp INTEGER
  )""";

  static Future<void> createTable(Database db) async {
    return db.execute(tableSql); // 使用Database的execute执行sql语句
  }

  // 插入记录数据
  Future<int> insert(MeritRecord record) {
    return database.insert(
        tableName,
        record.toJson(),
      conflictAlgorithm: ConflictAlgorithm.replace
    );
  }

  // 读取数据
  Future<List<MeritRecord>> query() async {
    List<Map<String,Object?>> data = await database.query(tableName);

    print(data);

    return data.map((e) => MeritRecord(
        id: e['id'].toString(),
        timeStamp: e['timestamp'] as int,
        value: e['value'] as int,
        image: e['image'].toString(),
        audio: e['audio'].toString()
    ))
        .toList();
  }
}