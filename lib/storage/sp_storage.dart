
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

const String kGuessSpKey = 'guess-config';
const String kMuyuSpKey = 'muyu-config';
const String kPaperKey = 'paper-config';

class SpStorage {
  SpStorage._();// 私有化构造

  static SpStorage? _storage;

  // 提供实例对象的访问途径
  static SpStorage get instance {
    _storage = _storage ?? SpStorage._();
    return _storage!;
  }

  SharedPreferences?  _sp;

  Future<void> initSpWhenNull() async {
    if (_sp != null) return;
    _sp = _sp ?? await SharedPreferences.getInstance();
  }

  // 存储
  Future<bool> saveGuessConfig({
     bool? guessing,
     int? value
}) async {
    await initSpWhenNull();
    String content = json.encode({'guessing': guessing,'value': value}); // 这里通过 json.encode 方法将 Map 对象编码成字符串
    return _sp!.setString(kGuessSpKey, content);
  }

  // 读取
  Future<Map<String,dynamic>> readGuessConfig() async {
    await initSpWhenNull();
    String content = _sp!.getString(kGuessSpKey) ?? "{}";
    return json.decode(content);
  }

  Future<bool> saveMuyuConfig({
    required int counter,
    required int activeImageIndex,
    required int activeAudioIndex
}) async {
    await initSpWhenNull();
    String content = json.encode({
      'counter':counter,
      'activeImageIndex': activeImageIndex,
      'activeAudioIndex': activeAudioIndex
    });
    return _sp!.setString(kMuyuSpKey, content);
  }

  Future<Map<String,dynamic>> readMuyuConfig() async {
    await initSpWhenNull();
    String content = _sp!.getString(kMuyuSpKey) ?? '{}';
    return json.decode(content);
  }

  Future<bool> savePaperConfig({
    required int activeColorIndex,
    required int activeStorkWidthIndex
}) async {
    await initSpWhenNull();
    String content = json.encode({
      'activeColorIndex': activeColorIndex,
      'activeStorkWidthIndex': activeStorkWidthIndex
    });
    return _sp!.setString(kPaperKey, content);
  }

  Future<Map<String,dynamic>> readPaperConfig() async{
    await initSpWhenNull();
    String content = _sp!.getString(kPaperKey) ?? '{}';
    return json.decode(content);
  }
}

