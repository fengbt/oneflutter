/*
 * @Author: fbt fbtgold@foxmail.com
 * @Date: 2023-11-29 14:58:04
 * @LastEditors: fbt fbtgold@foxmail.com
 * @LastEditTime: 2023-11-30 10:29:18
 * @FilePath: \untitled\lib\guess\guess_page.dart
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:untitled/storage/sp_storage.dart';

import 'guess_app_bar.dart';
import 'result_notice.dart';

class GuessPage extends StatefulWidget {
  const GuessPage({super.key});

  @override
  State<GuessPage> createState() => _GuessPageState();
}

class _GuessPageState extends State<GuessPage> with AutomaticKeepAliveClientMixin,SingleTickerProviderStateMixin {

  //
  late AnimationController controller;

  @override
  void initState(){
    super.initState();
    controller = AnimationController(
        vsync: this,// 需要SingleTickerProviderStateMixin
      duration: const Duration(milliseconds: 200)
    );
    _initConfig();
  }

  // 从SpStorage读取配置
  void _initConfig() async {
    Map<String,dynamic> config = await SpStorage.instance.readGuessConfig();
    _guessing = config['guessing']?? false;
    _randomNumber = config['value'] ?? 0;

    setState(() {

    });
  }

  int _randomNumber = 0;

  bool _guessing = false;
  bool? _isBig;

  final Random _random = Random();

  // 输入控制器有销毁的方法，需要覆写状态类的 dispose 方法
  @override
  void dispose(){
    _guessCtrl.dispose();
    super.dispose();
  }

  @override
  void _generateRandomNumberValue() {
    setState(() {
      _guessing = true;
      _randomNumber = _random.nextInt(100);
      // SpStorage.instance构造实例
      SpStorage.instance.saveGuessConfig(guessing: _guessing,value: _randomNumber);//存储数据
      print(_randomNumber);
    });
  }

  void _onCheck(){
    print("===Check:目标数值：$_randomNumber===${_guessCtrl.text}====");
    
    int? guessValue = int.tryParse(_guessCtrl.text);
    
    if(!_guessing || guessValue == null) {
      print('游戏未开始，或者输入非整数，无视');
      return;
    }

    // 没猜错
    if(guessValue == _randomNumber) {
      setState(() {
        _isBig = null;
        _guessing = false;
        SpStorage.instance.saveGuessConfig(guessing: _guessing,value: _randomNumber);//存储数据
      });
      return;
    }

    // 猜错了
      setState(() {
        _isBig = guessValue > _randomNumber;
      });



  }

  final TextEditingController _guessCtrl = TextEditingController();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: GuessAppBar(
          onCheck: _onCheck,
        controller: _guessCtrl,
      ),
      body: Stack(
        children: [
          if(_isBig != null)
          Column(
            children: [
              if(_isBig!)ResultNotice(color: Colors.redAccent, info: '大了啊'),
              Spacer(),
              if(!_isBig!)ResultNotice(color: Colors.blueAccent, info: '小了啊')
            ],
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                if(!_guessing)
                const Text('点击生成随机数值'),
                Text(_guessing ? '**' : '$_randomNumber',
                  style: Theme.of(context).textTheme.headlineMedium,
                )
              ],
            ),
          )
        ],

      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _guessing ? null : _generateRandomNumberValue,
        backgroundColor: _guessing ? Colors.grey : Colors.blue,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
