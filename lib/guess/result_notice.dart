
import 'package:flutter/material.dart';

class ResultNotice extends StatefulWidget{
  final Color color;
  final String info;

  const ResultNotice({
    Key? key,
    required this.color,
    required this.info
  }) : super(key: key);

  @override
  State<ResultNotice> createState() => _ResultNoticeState();
}

// 将状态类通过 with 关键字混入 SingleTickerProviderStateMixin, 让状态类拥有创建 Ticker 的能力
class _ResultNoticeState extends State<ResultNotice> with SingleTickerProviderStateMixin {

  late AnimationController controller;

  // state生命周期回调：处理状态类中成员对象的初始化
  @override
  void initState(){
    super.initState();
    controller = AnimationController(
        vsync: this,
        duration: const Duration(microseconds: 200)
    );
    controller.forward();
  }

  // state生命周期回调：
  @override
  void dispose(){
    controller.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant ResultNotice oldWidget){
    controller.forward(from: 0);
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Container(
          alignment: Alignment.center,
          color: widget.color,
          //  将 AnimatedBuilder 套在 Text 组件之上。
          //将动画控制器作为 animation 入参。
          //将需要动画变化的属性值，根据 animation.value 进行计算即可。
          child: AnimatedBuilder(
            animation: controller,
            builder: (_,child)=> Text(
              widget.info,
              style: TextStyle(
                fontSize: 54 * (controller.value),
                color: Colors.white,
                fontWeight: FontWeight.bold
              ),
            ),
          ),
        )
    );
  }
}