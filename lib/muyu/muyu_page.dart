
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flame_audio/flame_audio.dart';
import 'package:untitled/muyu/record_history.dart';
import 'package:untitled/storage/db_storage/db_storage.dart';
import 'package:untitled/storage/sp_storage.dart';
import 'package:uuid/uuid.dart';
import 'package:untitled/muyu/options/select_image.dart';
import 'models/merit_record.dart';
import 'options/select_audio.dart';

import 'animate_text.dart';
import 'models/image_option.dart';
import 'models/audio_option.dart';
import 'muyu_image.dart';
import 'count_panel.dart';
import 'muyu_app_bar.dart';

class MuyuPage extends StatefulWidget {
  const MuyuPage({Key? key}): super(key: key);

  @override
  State<MuyuPage> createState() => _MuyuPageState();
}

class _MuyuPageState extends State<MuyuPage> with AutomaticKeepAliveClientMixin {
  int _counter = 0;
  int _cruValue = 0;
  int _activeImageIndex = 0;
  int _activeAudioIndex = 0;
  MeritRecord? _cruRecord; // 敲击详情

  AudioPool? pool;
  final Random _random = Random();

  final Uuid uuid = Uuid();// 使用uuid库，用于生成唯一id

  List<MeritRecord> _records = []; // 敲击记录
  // 可选择的木鱼数组配置
  final List<ImageOption> imageOptions = const [
    ImageOption('基础版', 'assets/images/muyu.png', 1, 3),
    ImageOption('豪华版', 'assets/images/muyu2.png', 3, 6)
  ];

  final List<AudioOption> audioOptions = const [
    AudioOption('音效一', 'muyu_1.mp3'),
    AudioOption('音效二', 'muyu_2.mp3'),
    AudioOption('音效三', 'muyu_3.mp3'),
  ];
  
  @override
  void initState(){
    super.initState();
    _initAudioPool();
    _initConfig();
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: MuyuAppBar(onTapHistory: _toHistory,),
      body: Column(
        children: [
          Expanded(
            child: CountPanel(
              count: _counter,
              onTapSwitchAudio: _onTapSwitchAudio,
              onTapSwitchImage: _onTapSwitchImage,
            ),
          ),
          Expanded(
            child: Stack( // 叠放
              alignment: Alignment.topCenter, // 上方中间
              children: [
                MuyuAssetsImage(
                  image: activeImage,
                  onTap: _onKnock,
                ),
                if(_cruRecord != null ) AnimateText(record: _cruRecord!)
              ],
            )
          )
        ],
      ),
    );
  }

  void _initAudioPool() async {
    pool = await FlameAudio.createPool(
      'muyu_1.mp3',// 其中第一个参数是资源的名称，flame_audio 默认将本地资源放在 assets/audio 中，指定资源时直接写文件名即可。
      maxPlayers: 4,
    );
  }

  // 获取配置
  void _initConfig() async {
    Map<String,dynamic> config = await SpStorage.instance.readMuyuConfig();
    _counter = config['counter'] ?? 0;
    _activeImageIndex = config['activeImageIndex'] ?? 0;
    _activeAudioIndex = config['activeAudioIndex'] ?? 0;
    _records = await DbStorage.instance.meritRecordDao.query(); //从数据库里读取敲击记录
    setState(() {

    });
  }

  // 查看敲击历史
  void _toHistory(){
    Navigator.of(context).push( // 页面跳转
      MaterialPageRoute( //跳转页面渲染
          builder: (_) => RecordHistory( // 构建跳转到的界面
              records: _records.reversed.toList() // 反转记录循序
          )
      )
    );
  }

  // 敲木鱼
  void _onKnock(){
    pool?.start();

    // print('敲了一下');
    setState(() {
      _cruValue = knockValue; // 敲击增加的数目
      _counter += _cruValue; // 总数

      // 添加功德记录
      String id = uuid.v4();

      _cruRecord = MeritRecord(
          id: id,
          timeStamp: DateTime.now().millisecondsSinceEpoch,
          value: _cruValue,
          image: activeImage,
          audio: audioOptions[_activeAudioIndex].name
      );
      saveConfig();//保存修改
      DbStorage.instance.meritRecordDao.insert(_cruRecord!);// 点击时存入数据库
      // 添加功德记录
      _records.add(_cruRecord!);
    });
  }

  // 选择木鱼
  void _onTapSwitchImage(){
    showCupertinoModalPopup( // 底部弹窗
        context: context,
        builder: (BuildContext context){
          return ImageOptionPanel( // 返回的是弹出内容
              imageOptions: imageOptions, // 传入所有的木鱼配置
              onSelect: _onSelectImage, // 选择后的回调
              activeIndex: _activeImageIndex // 激活的木鱼序列
          );
        }
    );
  }

  // 选择音效
  void _onTapSwitchAudio(){
    showCupertinoModalPopup(
        context: context,
        builder: (BuildContext context){
          return AudioOptionPanel(
              audioOptions: audioOptions,
              activeIndex: _activeAudioIndex,
              onSelect: _onSelectAudio
          );
        }
    );
  }

  // 用spStorage保存
  void saveConfig(){
    SpStorage.instance.saveMuyuConfig(
        counter: _counter,
        activeImageIndex: _activeImageIndex,
        activeAudioIndex: _activeAudioIndex
    );
  }

  void _onSelectImage(int value){
    Navigator.of(context).pop(); // 关闭底部弹窗
    if(value == _activeImageIndex) return;  // 没有修改木鱼直接结束
    setState(() { // 修改激活木鱼的序号
      _activeImageIndex = value;
      saveConfig();//保存修改
    });
  }

  String get activeAudio => audioOptions[_activeAudioIndex].src;

  void _onSelectAudio(int value) async {
    Navigator.of(context).pop(); // 关闭底部弹窗
    if(value == _activeAudioIndex) return;  // 没有修改音效直接结束
      _activeAudioIndex = value;// 修改激活音效的序号
    saveConfig();//保存修改

    pool = await FlameAudio.createPool(
        activeAudio,
        maxPlayers: 1
    );
  }

  // get方法，类似vue的computed方法，通过imageOptions和_activeImageIndex获取activeImage
  String get activeImage => imageOptions[_activeImageIndex].src;

  // 同上
  int get knockValue {
    int min = imageOptions[_activeImageIndex].min;
    int max = imageOptions[_activeImageIndex].max;
    return min + _random.nextInt(max+1 - min);
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}