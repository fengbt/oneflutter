
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:untitled/muyu/models/merit_record.dart';

DateFormat format = DateFormat('yyyy年MM月dd日 HH:mm:ss');

class RecordHistory extends StatelessWidget {
  final List<MeritRecord> records;

  const RecordHistory({Key? key, required this.records}):super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      // column有范围，当超出尺寸范围时无法显示
      // body: Column(
        // children: List.generate(records.length, (index) => _buildItem(context, index)),
      //),
      body: ListView.builder( // 为了实现滑动，使用ListView.builder实现构建滑动列表
        itemBuilder: _buildItem,
        itemCount: records.length,
      ),
    );
  }

  PreferredSizeWidget _buildAppBar()=>
      AppBar(
        iconTheme: const IconThemeData(color: Colors.black),
        centerTitle: true,
        title: const Text(
          '功德记录',
          style: TextStyle(color: Colors.black,fontSize: 16),
        ),
        elevation: 0,
        backgroundColor: Colors.white,
      );

  Widget _buildItem(BuildContext context,int index) {
    MeritRecord merit = records[index];
    String date = format.format(DateTime.fromMillisecondsSinceEpoch(merit.timeStamp));

    return ListTile(
      leading: CircleAvatar(// leading:左侧组件 ；使用CircleAvatar展示圆形图形
        backgroundColor: Colors.blue,
        backgroundImage: AssetImage(merit.image),
      ),
      title: Text('功德+${merit.value}'), // title:标题组件
      subtitle: Text(merit.audio), // subtitle: 副标题组件
      trailing: Text( // trailing: 尾部组件
        date,
        style: const TextStyle(
          fontSize: 12, color: Colors.grey
        ),
      ),
    );
  }
}