
import 'package:flutter/cupertino.dart';
import 'package:untitled/muyu/models/merit_record.dart';

class AnimateText extends StatefulWidget{
  final MeritRecord record;

  const AnimateText({Key?key, required this.record}):super(key: key);

  @override
  State<AnimateText> createState() =>_FadTextState();
}

class _FadTextState extends State<AnimateText> with SingleTickerProviderStateMixin {
  late AnimationController controller;
  late Animation<double> opacity;
  late Animation<Offset> position;
  late Animation<double> scale;
  
  @override
  void initState(){
    super.initState();
    controller = AnimationController(vsync: this,duration: Duration(milliseconds: 500));
    opacity = Tween(begin: 1.0,end: 0.0).animate(controller);
    scale = Tween(begin: 1.0,end: 0.9).animate(controller);
    // 在竖直方向上的偏移量，从两倍子组件高度变化到0
    position = Tween(begin: const Offset(0, 2),end: Offset.zero).animate(controller);

    controller.forward();
  }

  @override
  void didUpdateWidget(covariant AnimateText oldWidget){
    super.didUpdateWidget(oldWidget);
    // 通过判断id是否变化来阻止动画在修改木鱼图片时触发启动
    if(oldWidget.record.id != widget.record.id)controller.forward(from: 0);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ScaleTransition( // 缩放动画
        scale: scale,
        child: SlideTransition( // 移动
          position: position,
          child: FadeTransition( // 透明度控制
            opacity: opacity,
            child: Text('功德+${widget.record.value}'),
          ),
        ),
    );
  }
}