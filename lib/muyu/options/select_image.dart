
import 'package:flutter/material.dart';
import 'package:untitled/muyu/models/image_option.dart';

class ImageOptionPanel extends StatelessWidget {
  final List<ImageOption> imageOptions;
  final ValueChanged<int> onSelect;
  final int activeIndex;

  const ImageOptionPanel({
    Key? key,
    required this.imageOptions,
    required this.onSelect,
    required this.activeIndex
}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const TextStyle labelStyle = TextStyle(fontSize: 16,fontWeight: FontWeight.bold);
    const EdgeInsets padding = EdgeInsets.symmetric(
        horizontal: 8.0, // 左右填充
        vertical: 16 // 上下填充
    );

    return Material(
      child: SizedBox(
        height: 300,
        child: Column(
          children: [
            Container(
              height: 46,
              alignment: Alignment.center,
              child: const Text("选择木鱼",style: labelStyle,),
            ),
            Expanded(
                child:Padding(
                  padding: padding,
                  child: Row(
                    children: [
                      Expanded(child: _buildByIndex(0)),
                      const SizedBox(width: 10,),
                      Expanded(child: _buildByIndex(1))
                    ],
                  ),
                )
            )
          ],
        ),
      ),
    );
  }

  Widget _buildByIndex(int index) {
    bool active = index == activeIndex; // 判断是否激活
    return GestureDetector(// 手势控制组件
      onTap: ()=>onSelect(index), // 选择木鱼的回调函数
      child: ImageOptionItem(
        option: imageOptions[index],
        active: active, //
      ),
    );
  }
}

// 选择木鱼的模块
class ImageOptionItem extends StatelessWidget {
  final ImageOption option;
  final bool active;

  const ImageOptionItem({
    Key? key
    , required this.option,
    required this.active
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const Border activeBorder = Border.fromBorderSide(BorderSide(color: Colors.blue));
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 8), //EdgeInsets：四边填充，vertical指的是上下
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: !active ? null: activeBorder,
      ),
      child: Column(// 上中下结构；使用竖向排列
        children: [
          Text(option.name,style: TextStyle(fontWeight: FontWeight.bold),),
          Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Image.asset(option.src),
              )
          ),
          Text(
              '每次功德 +${option.min}~${option.max}',
            style: TextStyle(fontSize: 12,color: Colors.grey
            ),
          )
        ],
      ),
    );
  }


}