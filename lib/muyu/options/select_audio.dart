
import 'package:flutter/material.dart';
import 'package:flame_audio/flame_audio.dart';

import '../models/audio_option.dart';

class AudioOptionPanel extends StatelessWidget{
  final List<AudioOption> audioOptions;
  final int activeIndex;
  final ValueChanged<int> onSelect;

  const AudioOptionPanel({
    Key? key,
    required this.audioOptions,
    required this.activeIndex,
    required this.onSelect
}): super(key: key);

  @override
  Widget build(BuildContext context){
    const TextStyle labelStyle = TextStyle(fontSize: 16, fontWeight: FontWeight.bold);
    return Material(
      child: SizedBox(
        height: 300,
        child: Column(
          children: [
            Container(
              height: 46,
              alignment: Alignment.center,
              child: const Text('选择音效',style: labelStyle,),
            ),
            ...List.generate(audioOptions.length, _buildByIndex)// 要构造的数量；要构建的模型函数
          ],
        ),
      ),
    );
  }

  // 音效
  Widget _buildByIndex(int index){
    bool active = index == activeIndex;
    return ListTile(
      selected: active,//是否激活
      onTap: ()=> onSelect(index),
      title: Text(audioOptions[index].name),
      trailing: IconButton( // 列表尾部
        splashRadius: 20, // 点击后的动态范围
        onPressed: ()=>_tempPlay(audioOptions[index].src), // 点击触发
        icon: Icon( //
          Icons.record_voice_over_rounded,
          color: Colors.blue,
        ),
      ),
    );
  }

  //根据传入的路径加载静态资源
  void _tempPlay(String src) async {
    AudioPool pool = await FlameAudio.createPool(src,maxPlayers:4);
    pool.start();
  }
}