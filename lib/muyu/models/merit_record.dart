class MeritRecord{
  final String id; // 唯一的id
  final int timeStamp; // 记录的时间戳
  final int value; // 功德数
  final String image; // 图片资源
  final String audio;// 音效资源

  MeritRecord({
    required this.id,
    required this.timeStamp,
    required this.value,
    required this.image,
    required this.audio
  });

  // 提供一个toJson方法，便于将对象转为Map
  Map<String,dynamic> toJson() => {
    "id": id,
    "timestamp": timeStamp,
    "value": value,
    "image": image,
    "audio": audio
  };

}