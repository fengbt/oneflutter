
 // 木鱼种类
class ImageOption {
  final String name; // 名称
  final String src; // 图片资源
  final int min; // 每次点击增加最小数
  final int max; // 每次点击增加最大数

  const ImageOption(
      this.name,
      this.src,
      this.min,
      this.max
      );
}