
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:untitled/paper/color_selector.dart';
import 'package:untitled/paper/conform_dialog.dart';
import 'package:untitled/paper/paper_app_bar.dart';
import 'package:untitled/paper/stork_width_selector.dart';
import 'package:untitled/storage/sp_storage.dart';

import 'model.dart';

class Paper extends StatefulWidget {
  const Paper({Key? key}):super(key: key);

  @override
  State<Paper> createState() => _PaperState();
}

class _PaperState extends State<Paper> with AutomaticKeepAliveClientMixin {
  List<Line> _lines = []; // 线列表

  int _activeColorIndex = 0; // 颜色激活索引
  int _activeStorkWidthIndex = 0; // 线宽激活索引

  //支持的颜色
  final List<Color> supportColors = [
    Colors.black,
    Colors.red,
    Colors.orange,
    Colors.yellow,
    Colors.green,
    Colors.blue,
    Colors.indigo,
    Colors.purple,
    Colors.pink,
    Colors.grey,
    Colors.redAccent,
    Colors.orangeAccent,
    Colors.yellowAccent,
    Colors.greenAccent,
    Colors.blueAccent,
    Colors.indigoAccent,
    Colors.purpleAccent,
    Colors.pinkAccent,
  ];

  // 支持的线粗
  final List<double> supportStorkWidths = [1,2,4,6,8,10];

  // 历史线组
  List<Line> _historyLines = [];

  void _back(){
    Line line = _lines.removeLast();
    _historyLines.add(line);
    setState(() {

    });
  }

  void _revocation(){
    Line line = _historyLines.removeLast();
    _lines.add(line);
    setState(() {

    });
  }

  @override
  void initState() {
    super.initState();

    initConfig();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PaperAppBar(
        onClear: _showClearDialog,
        onBack: _lines.isEmpty ? null : _back,
        onRevocation: _historyLines.isEmpty ? null : _revocation,
      ),
      body: Stack(
        children: [
          GestureDetector( // 监听拖拽事件
            onPanStart: _onPanStart, // 用户开始拖拽的事件
            onPanUpdate: _onPanUpdate, // 用户拖拽中的事件
            child: CustomPaint(
              painter: PaperPainter(
                  lines: _lines
              ),
              child: ConstrainedBox(constraints: const BoxConstraints.expand(),),
            ),
          ),
          Positioned(
            bottom: 0,
            width: MediaQuery.of(context).size.width,
            child: Row(
              children: [
                Expanded(
                    child: ColorSelector(
                      supportColors: supportColors,
                      activeIndex: _activeColorIndex,
                      onSelect: _onSelectColor,
                    )
                ),
                Expanded(
                    child: StorkWidthSelector(
                      supportStorkWidths: supportStorkWidths,
                      activeIndex: _activeStorkWidthIndex,
                      color: supportColors[_activeColorIndex],
                      onSelect: _onSelectStorkWidth,
                    )
                )
              ],
            ),
          )
        ],
      )
    );
  }

  void initConfig() async {
    Map<String,dynamic> config = await SpStorage.instance.readPaperConfig();
    _activeColorIndex = config['activeColorIndex'] ?? 0;
    _activeStorkWidthIndex = config['activeStorkWidthIndex'] ?? 0;
    setState(() {

    });
  }

  // 清空弹窗
  void _showClearDialog(){
    String msg = "您的当前操作会清空绘制内容，是否确定删除!";
    showDialog(
        context: context,
        builder: (ctx) => ConformDialog(
            title: '清空提示',
            msg: msg,
            onConform: _clear,
            conformText: '确定',
        )
    );
  }

  // 清空绘版
  void _clear(){
    _lines.clear();
    _historyLines.clear();
    Navigator.of(context).pop();

    // 页面刷新
    setState(() {

    });
  }

  // 拖拽开始，添加新线及线宽、线色
  void _onPanStart(DragStartDetails details) {
    _lines.add(
        Line(
          points: [details.localPosition],
          strokeWidth: supportStorkWidths[_activeStorkWidthIndex],
          color: supportColors[_activeColorIndex]
        )
    );
  }

  // 拖拽中，为新线添加点
  void _onPanUpdate(DragUpdateDetails details) {
    Offset point = details.localPosition; // 新的点

    double distance = (_lines.last.points.last - point).distance;//当前点和最后一点的距离

    // 通过判断两个临近点的距离，减少点的数量，降低绘制压力
    if(distance > 5){ // 超过5这个值即可添加更新
      _lines.last.points.add(details.localPosition);// 给最后的线添加点

      if(_historyLines.isNotEmpty)_historyLines.clear();

      setState(() {});
    }

  }

  // 更新选中的线宽
  void _onSelectStorkWidth(int index){
    if(index != _activeStorkWidthIndex){
      setState(() {
        _activeStorkWidthIndex = index;
        savePaper();
      });
    }
  }

  // 更新选中的线色
  void _onSelectColor(int index){
    if(index != _activeColorIndex){
      setState(() {
        _activeColorIndex = index;
        savePaper();
      });
    }
  }

  void savePaper(){
    SpStorage.instance.savePaperConfig(activeColorIndex: _activeColorIndex, activeStorkWidthIndex: _activeStorkWidthIndex);
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}

/// 绘画逻辑
class PaperPainter extends CustomPainter {
  PaperPainter({
      required this.lines
  }){
      _paint = Paint()
          ..style = PaintingStyle.stroke
          ..strokeCap = StrokeCap.round;
    }

    late Paint _paint;
    final List<Line> lines;

    @override
    void paint(Canvas canvas, Size size) {
      for(int i = 0; i < lines.length;i++){
        drawLine(canvas, lines[i]);
      }
    }

    ///根据点位绘制线
    void drawLine(Canvas canvas,Line line) {
      _paint.color = line.color;
      _paint.strokeWidth = line.strokeWidth;
      // 通过drawPoints绘制pointMode.polygon类型的点集
      canvas.drawPoints(PointMode.polygon, line.points, _paint);
    }

    @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}